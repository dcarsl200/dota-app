var gulp = require('gulp');
var $    = require('gulp-load-plugins')();
var Promise = require('es6-promise').Promise;

gulp.task('sass', function() {
    return gulp.src('./css/scss/app.scss')
      .pipe($.sass()
        .on('error', $.sass.logError))
      .pipe($.autoprefixer({
        browsers: ['last 4 versions', 'ie >= 9']
      }))
      .pipe(gulp.dest('./css/dist')) 
});

gulp.task('default', ['sass'], function() {
    gulp.watch(['./css/scss/**/*.scss'], ['sass']);
});