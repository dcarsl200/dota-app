import React from "react"

// Redux
import { connect } from "react-redux"

// Routing
import { BrowserRouter, Route, Switch } from 'react-router-dom'

//Components
import ApplicationTitleContainer from './containers/ApplicationTitleContainer';

// Modules
import Home from "./modules/Home"
import HeroPage from "./modules/Hero"

// Actions
import { fetchHeroes } from "./actions/heroesActions"

@connect()

export default class App extends React.Component {

    constructor(props) {
        super(props);
        // Load initial async data set in root app component
        this.props.dispatch(fetchHeroes())
    }

    render() {
      return (
            <div className="application_wrap">
                <ApplicationTitleContainer /> 
                <BrowserRouter>
                    <Switch>               
                        <Route exact path="/" component={Home}/>
                        <Route path="/hero/:heroName/:heroID" component={HeroPage}/>
                    </Switch>
                </BrowserRouter>
            </div>    
      );
    }
}
