import React from "react"

// Third Party
import BackgroundImage from 'react-background-image-loader'

// Utils
import { formatHeroAttr, fetchHeroImageURL } from '../utils/heroUtil'
import { formatForUrl } from '../utils/stringFormatting'

// Routing
import { NavLink, withRouter } from 'react-router-dom'

export default class HeroListItem extends React.Component {
    render() {
        const { hero } = this.props;
        const localImage = '../../images/loading_bg.jpg';
    
        return <li className="block_list_li_primary">
            <BackgroundImage className="background_image_plugin" src={fetchHeroImageURL(hero.id)} placeholder={localImage}></BackgroundImage>     
            <h4 class="title title--h4">{hero.localized_name}</h4>
            <div>
                <span className={'stat-block stat-block--' + hero.primary_attr}>{formatHeroAttr(hero.primary_attr)}</span>
                <span className={ 'stat-block stat-block--plain stat-block--' + hero.attack_type}>{hero.attack_type}</span>
            </div>
            <ul className="horizontal_bar_list">
                {
                    hero.roles.map(role => 
                        <li key={role} className="horizontal_bar_list_li">{ role }</li>
                    )
                }
            </ul>
            <NavLink className="overlay_link" to={'/hero/' + formatForUrl(hero.localized_name) + '/' + hero.id }></NavLink>
        </li>

    }
}
