import React from "react"

export default class ApplicationTitleBlock extends React.Component {

    render() {
        const { title, logo } = this.props;
        return <div className="title_container grid_container">
                <h1 className="title">
                    <img src={ logo } alt="React Dota Logo" />
                    { title }
                </h1>
            </div>
    } 
}
