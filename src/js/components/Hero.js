import React from "react"

// Third Party
import BackgroundImage from 'react-background-image-loader'

// Utils
import { formatHeroAttr, fetchHeroImageURL } from '../utils/heroUtil'
import { formatForUrl } from '../utils/stringFormatting'

// Routing
import { NavLink } from 'react-router-dom'

export default class Hero extends React.Component {

    render() {
        const { hero } = this.props;
        const localImage = '../../images/loading_bg.jpg';

        return <div className="grid_container hero_page">
            <NavLink className="basic_link" to={'/'}>Return to Heroes</NavLink>            
            <div class="hero_image">
                <BackgroundImage className="background_image_plugin" src={fetchHeroImageURL(hero.id)} placeholder={localImage}></BackgroundImage>                
            </div>
            <div class="hero_image__text_content">
                <h1>{hero.localized_name}</h1>
                <div>
                    <span className={'stat-block stat-block--' + hero.primary_attr}>{formatHeroAttr(hero.primary_attr)}</span>
                    <span className={ 'stat-block stat-block--plain stat-block--' + hero.attack_type}>{hero.attack_type}</span>
                </div>
                <ul className="horizontal_bar_list">
                    {
                        hero.roles.map(role => 
                            <li key={role} className="horizontal_bar_list_li">{ role } / </li>
                        )
                    }
                </ul>
            </div>
        </div>

    }
}
