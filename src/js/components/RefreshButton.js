import React from "react"


export default class RefreshButton extends React.Component {

    render() {
        return <div class="refresh_wrap">
            <button class="button button_refresh">Refresh Heroes</button>
        </div>

    }
}
