import React from "react"

import { getFirstPropertyName } from "../utils/arrayUtil"

export default class Filter extends React.Component {

    render() {
        const { name, options, placeholder, currentFilter } = this.props;
        const mappedOptions = options.map(option => {
            return <option key={option[0] + Math.random() * 10} value={option[0]}>
                {option[1]}
            </option>
        })

        return <div class="filter-list__select-filter">
            <select name={name} value={currentFilter} onChange={this.props.changeFilter}>
                <option value="">{placeholder}</option>
                { mappedOptions }
            </select>
        </div>
    } 
}
