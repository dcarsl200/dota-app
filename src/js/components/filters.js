import React from "react"

import Filter from "./filter";

import { getFirstPropertyName } from "../utils/arrayUtil"

export default class Filters extends React.Component {

    render() {
        const { filters, appliedFilters } = this.props
        const mappedFilters = filters.map(filter => {
                //not a big fan of this...
                for(var appliedFilter of appliedFilters){
                    if(getFirstPropertyName(appliedFilter) === filter.name){
                        var currentFilter = appliedFilter[getFirstPropertyName(appliedFilter)];
                    }
                }
                return <Filter key={filter.name} currentFilter={currentFilter} name={filter.name} placeholder={filter.placeholder} changeFilter={this.props.changeFilter} options={filter.options} />
            }
        )

        return <div class="filter-list">
            {mappedFilters}
            <button onClick={this.props.clearFilters}>Clear Filters</button>
        </div>
    } 
}
