import React from "react"

// Components
import HeroListItem from './HeroListItem'
import FiltersContainer from '../containers/filtersContainer'

export default class HeroList extends React.Component {

    render() {
        const { heroes } = this.props;

        const mappedHeroes = heroes.map(hero =>
            <HeroListItem key={hero.id} hero={hero} />
        )

        return <div className="list_container grid_container">
            <FiltersContainer/>
            <ul className="block_list block_list--hero">{mappedHeroes}</ul>
        </div>

    }
}
