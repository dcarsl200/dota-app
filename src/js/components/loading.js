import React from "react"

export default class LoadingSpinner extends React.Component {
    render() {
        const { width } = this.props;
        const customStyle = {
            width: width,
            height: width
        }
        return <div class="loading-element">
            <div class="loading-element__spinner" style={customStyle}>
                <div></div><div></div><div></div><div></div>
            </div>
        </div>
    } 
}
