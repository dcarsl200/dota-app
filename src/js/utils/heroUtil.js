export function formatHeroAttr(attr) {
    switch (attr) {
        case 'agi': {
            return 'Agility'
        }
        case 'str': {
            return 'Strength'
        }
        case 'int': {
            return 'Intelligence'
        }
            return;
    }
}

export function fetchHeroImageURL(heroID) {
    return location.protocol + '//' + location.host + '/images/hero_images/' + heroID + '.jpg';
}