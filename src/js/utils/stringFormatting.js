
export function formatForUrl(str){
    str = str.replace(' ', '-').toLowerCase();
    return encodeURIComponent(str);
}