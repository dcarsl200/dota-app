import React from "react"

// Redux
import { connect } from "react-redux"

// Components
import Filters from "../components/filters"

// Utils
import { formatHeroAttr } from '../utils/heroUtil'
import { isArrayInArray } from '../utils/arrayUtil'

import { addFilter, removeFilter, clearAllFilters} from '../actions/filterActions'
@connect((store) => {
  return {
    appliedFilters: store.appliedFilters.appliedFilters,
    heroes: store.heroes.heroes
  };
})

export default class FiltersContainer extends React.Component {

    handleChange(e){
      const name = e.target.name
      if(e.target.value){
        const value = e.target.value
        const obj = {[name] : value}
        return this.props.dispatch(addFilter(obj))
      }
      return this.props.dispatch(removeFilter(name))
    }

    clearFilters(e){
      return this.props.dispatch(clearAllFilters(e))
    }

    render() {
        const { appliedFilters, heroes } = this.props;
        let filters = [
            {
              name : "primary_attr",
              placeholder : "Hero Type",
              options : []
            },
            {
              name: "attack_type",
              placeholder: "Attack Type",
              options : []
            },
            {
              name : "roles",
              placeholder : "Role",
              options : [] 
            },
            {
              name: "localized_name",
              placeholder: "Hero Name",
              options : []
            }
          ];

        // Builds filters based on data set - this kinda sucks
        // Maybe this should be stored in the state instead of being
        // built every single render
        for (var hero of heroes) {

          const roles = hero.roles
          const primary_attr = [hero.primary_attr, formatHeroAttr(hero.primary_attr)]
          const attack_type = [hero.attack_type, hero.attack_type]
          const localized_name = [ hero.localized_name,  hero.localized_name ]
          
          !isArrayInArray(filters[0].options, primary_attr) ? filters[0].options.push(primary_attr) : false
          !isArrayInArray(filters[1].options, attack_type) ? filters[1].options.push(attack_type) : false
          !isArrayInArray(filters[3].options, localized_name) ? filters[3].options.push(localized_name) : false

          for (var role of roles) {
            !isArrayInArray(filters[2].options, [role,role]) ? filters[2].options.push([role,role]) : false               
          }
        }

        return <Filters filters={filters} appliedFilters={appliedFilters} changeFilter={this.handleChange.bind(this)} clearFilters={this.clearFilters.bind(this)} />
    }
  
}
