import React from "react"

// Redux
import { connect } from "react-redux"

// Actions
import { fetchHeroes } from "../actions/heroesActions"

// Routing
import { NavLink, withRouter } from 'react-router-dom'

//Components
import HeroList from "../components/HeroList"
import LoadingSpinner from "../components/loading"

import { getFirstPropertyName } from "../utils/arrayUtil"

@connect((store) => {
    return {
        heroes: store.heroes.heroes,
        appliedFilters: store.appliedFilters.appliedFilters
    };
})

export default class HeroListContainer extends React.Component {

    fetchHeroes() {
        this.props.dispatch(fetchHeroes())
    }

    render() {
        const { heroes, appliedFilters } = this.props;

        // This should maybe not be in this file
        let filteredHeroes = heroes.filter( 
            hero => appliedFilters.every( 
                filter => {
                    var key = Object.keys(filter)[0]

                    // Roles can be an array
                    if(getFirstPropertyName(filter) === 'roles'){
                        return hero[key].indexOf(filter[key]) !== -1;
                    }

                    return hero[key] == filter[key]                    
                }
            )
        )

        if(appliedFilters.length <= 0) filteredHeroes = heroes;

        if(heroes.length === 0) return <LoadingSpinner />

        if(filteredHeroes.length === 0) return <div>No Heroes Found <HeroList heroes={filteredHeroes} /></div>

        return <HeroList heroes={filteredHeroes} />
    }
}
