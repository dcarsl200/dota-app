import React from "react"

// Redux
import { connect } from "react-redux"

// Actions
import { updateTitle } from "../actions/applicationTitleActions"

// Components
import ApplicationTitleBlock from "../components/ApplicationTitleBlock"

@connect((store) => {
  return {
    title: store.appTitle.appTitle,
    logo: store.appTitle.appLogo,
  };
})

export default class ApplicationTitleContainer extends React.Component {

    render() {
        const { title, logo } = this.props;
        return <ApplicationTitleBlock title={title} logo={logo} />
    }
  
}
