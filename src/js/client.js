import React from "react"
import ReactDOM from "react-dom"
import App from "./root"
import store from "./store"
import { Provider } from "react-redux"

const app = document.getElementById('app')

ReactDOM.render(
<Provider store={store}>
  <App/>
</Provider>, app);
