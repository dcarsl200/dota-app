export default function reducer(state={
    appTitle: 'React Dota',
    appLogo: 'https://vignette.wikia.nocookie.net/defenseoftheancients/images/6/64/Dota_2_Logo_only.png'
  }, action) {

    switch (action.type) {
      case "UPDATE_TITLE": {
        return {...state, appTitle: action.payload}
      }
      case "UPDATE_LOGO": {
        return {...state, appLogo: action.payload}
      }
    }

    return state
}
