export default function reducer(state={
    heroes: [],
    fetching: false,
    fetched: false,
    error: null,
  }, action) {

    switch (action.type) {
      case "FETCH_HEROES": {
        return {...state, fetching: true}
      }
      case "FETCH_HEROES_REJECTED": {
        return {...state, fetching: false, error: action.payload}
      }
      case "FETCH_HEROES_FULFILLED": {
        return {
          ...state,
          fetching: false,
          fetched: true,
          heroes: action.payload,
        }
      }
    }

    return state
}
