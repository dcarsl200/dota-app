import { combineReducers } from "redux"

import heroes from "./heroesReducer"
import appTitle from "./applicationTitleReducer";
import appliedFilters from "./filtersReducer";

export default combineReducers({
  heroes,
  appTitle,
  appliedFilters
})
