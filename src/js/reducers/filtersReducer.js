import { getFirstPropertyName } from '../utils/arrayUtil'

export default function reducer(state={
    appliedFilters: []
  }, action) {

    switch (action.type) {
      case "ADD_FILTER": {
            return {
                ...state,
                appliedFilters: [...state.appliedFilters.filter(filter => getFirstPropertyName(filter) !== getFirstPropertyName(action.payload)), action.payload],
              }
        return {
            ...state
        }
      }
      case "REMOVE_FILTER": {
        return {...state, 
            appliedFilters: state.appliedFilters.filter(filter => getFirstPropertyName(filter) !== action.payload)}
      }
      case "CLEAR_ALL_FILTERS": {
        return{
          ...state,
          appliedFilters: []
        }
      }
    }

    return state
}
