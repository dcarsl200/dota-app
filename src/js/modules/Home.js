import React from "react"
// Redux
import { connect } from "react-redux"
// Components
import HeroListContainer from "../containers/HeroListContainer"

import { addFilter, removeFilter} from '../actions/filterActions'

export default class Home extends React.Component {
    render() {
        return <div className="home_page">
            <HeroListContainer />
        </div>
    }
}