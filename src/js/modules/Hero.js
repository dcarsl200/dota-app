import React from "react"

// Redux
import { connect } from "react-redux"

// Third Party
import BackgroundImage from 'react-background-image-loader'

//Components
import Hero from '../components/Hero';
import LoadingSpinner from '../components/loading';

@connect((store) => {
    return {
        heroes: store.heroes.heroes,
    };
})

export default class HeroPage extends React.Component {
    render() {
        const heroes = this.props.heroes
        if(heroes.length > 0){
            let hero = heroes.filter(hero => hero.id == this.props.match.params.heroID);
            hero = hero[0];
            return <Hero hero={hero} />
        }

        return <LoadingSpinner />
    }
}