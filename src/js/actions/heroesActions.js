import axios from "axios";

export function fetchHeroes() {
  return function(dispatch) {
    dispatch({type: "FETCH_HEROES"});
    axios.get("https://api.opendota.com/api/heroes")
      .then((response) => {
        dispatch({type: "FETCH_HEROES_FULFILLED", payload: response.data})
      })
      .catch((err) => {
        dispatch({type: "FETCH_HEROES_REJECTED", payload: err})
      })
  }
}

export function addHero(id, text) {
  return {
    type: 'ADD_HERO',
    payload: {
      id,
      text,
    },
  }
}

export function updateHero(id, text) {
  return {
    type: 'UPDATE_HERO',
    payload: {
      id,
      text,
    },
  }
}

export function deleteHero(id) {
  return { type: 'DELETE_HERO', payload: id}
}
