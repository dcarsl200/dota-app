export function updateTitle(title) {
  return {
    type: 'UPDATE_TITLE',
    payload: title,
  }
}

export function updateLogo(url) {
    return {
      type: 'UPDATE_LOGO',
      payload: url,
    }
  }
